package de.iccas.backend.file;

import com.vaadin.ui.Notification;
import de.iccas.Main;
import de.iccas.backend.model.*;
import de.iccas.backend.util.Log;
import de.iccas.backend.util.Util;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class GeNIeFileReader {

    private Document genieDoc;

    private File genieFile;

    private Main main;

    private Log log;

    private int[] numExamsPerGroup = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private List<Element> obsNodeList = null;

    public GeNIeFileReader(File genieFile, Main mainInstance) {
        genieDoc = null;
        this.genieFile = genieFile;
        this.main = mainInstance;
        this.log = main.log;

        if(!genieFile.exists()) {
            //show Warning
            Notification.show("File not found", "The GeNIe file which holds all data " +
                    "could not be found. Please try to solve this by changing the file path. This could be " +
                    "done through 'File->Preferences...'.", Notification.Type.ERROR_MESSAGE);
        }
    }

    /**
     * Reads the GeNIe file, parses the nodes into examination objects and assigns the corresponding group.
     * This also initializes the number of examinations in each group which is needed for tracking the progress.
     *
     * @return The list of examination groups with all examinations assigned to their groups.
     */
    public LinkedList<ExaminationGroup> readFileAndGetExamGroups() {
        log.info("Reading file: " + genieFile.getAbsolutePath());
        LinkedList<ExaminationGroup> exams = new LinkedList<ExaminationGroup>();
        //get all observable nodes, i.e. the nodes which can be observed and set by the user
        obsNodeList = getObservableNodes();
        //initialize the examination groups
        for(String s : Util.EXAMINATION_GROUPS) {
            exams.add(new ExaminationGroup(s));
        }
        //search through the examination group names to assign the correct examination group which was
        //initialized before
        for(Element e : obsNodeList) {
//            System.out.println("Searching group for " + e.getAttributeValue("id"));
            Examination exam = new Examination(e.getAttributeValue("id"), getPossibleFindings(
                    e.getAttributeValue("id")));
            int index = Util.EXAMINATION_GROUPS.size() - 1;
            for(String s : Util.EXAMINATION_GROUPS_GENIE) {
//                System.out.print("  in " + s + "? : ");
                //lower case is important!
                if(e.getAttributeValue("id").toLowerCase().contains(s)) {
                    //if the examination group was found add the examination with the name and the list of possible outcomes
                    index = Util.EXAMINATION_GROUPS_GENIE.indexOf(s);
//                    System.out.print("yes\n");
                    break;
                } else if(e.getAttributeValue("id").toLowerCase().contains(Util.PALPATION)) {
                    //if the examination group equals "palpation" add the examination with the name and the list of
                    //possible outcomes to the "clinical_exam" group
                    index = 1;
//                    System.out.print("no, but in palpation\n");
                    break;
                }
//                System.out.print("no\n");
            }
//            if(index == 9) {
//                System.out.print("no, assigning it to others\n");
//            }
            exams.get(index).addExamination(exam);
        }
        log.info("Found & loaded " + exams.size() + " examination groups.");
        return exams;
    }

    /**
     * Reads a GeNIe file and parses a JDOM document. The method returns a list with all
     * nodes of the graph represented by the GeNIe file.
     *
     * @return A list with all child nodes of the node "nodes", i.e. a list of all nodes of
     * the graph.
     */
    public List<Element> getNodes() {

        List<Element> nodeList = null;

        try {
            // initialize document
            SAXBuilder builder = new SAXBuilder();
            genieDoc = builder.build(genieFile);

            // The root node of the file should be "smile"
            Element rootNode = genieDoc.getRootElement();

            // Get the children of the root node with the name "nodes"
            // This is the tag holding all nodes of the graph.
            Element nodes = rootNode.getChild("nodes");

            nodeList = (List<Element>) nodes.getChildren();

        } catch (JDOMException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return nodeList;
    }

    /**
     * Reads a GeNIe file and parses a JDOM document. The method returns a list with all
     * nodes of the graph represented by the GeNIe file, that are observable. All other nodes,
     * i.e. nodes that are unobservable are marked through the tag:
     * "<property id="unobservable">true</property>"
     *
     * @return A list with all child nodes of the node "nodes", i.e. a list of all nodes of
     * the graph.
     */
    public List<Element> getObservableNodes() {

        List<Element> nodeList = null;
        if(obsNodeList == null) {
            obsNodeList = new LinkedList<Element>();

            try {
                // initialize document
                SAXBuilder builder = new SAXBuilder();
                genieDoc = builder.build(genieFile);

                // The root node of the file should be "smile"
                Element rootNode = genieDoc.getRootElement();

                // Get the children of the root node with the name "nodes"
                // This is the tag holding all nodes of the graph.
                Element nodes = rootNode.getChild("nodes");

                nodeList = (List<Element>) nodes.getChildren();
                for(Element e : nodeList) {
                    Element node = e.getChild("property");
                    if(node != null) {
                        if(!node.getAttribute("id").getValue().equals("unobservable")) {
                            obsNodeList.add(e);
                        }
                    } else {
                        obsNodeList.add(e);
                    }

                }

            } catch (JDOMException | IOException e) {
                //show Warning
                Notification.show("Error","An error has occured while loading date from file." +
                        "The detailed error message is:\n" + e,Notification.Type.ERROR_MESSAGE);
            }
            initNumExams(obsNodeList);
        }
        return obsNodeList;
    }

    /**
     *
     * @param obsNodeList
     */
    @Deprecated
    private void initNumExams(List<Element> obsNodeList) {
        for(Element e : obsNodeList) {
            //find group; init index with "Others..."
            int index = Progress.OTHERS_INDEX;
            for(String s : Util.EXAMINATION_GROUPS_GENIE) {
                if(e.getAttributeValue("id").toLowerCase().contains(s)) {
                    //group found
                    index = Util.EXAMINATION_GROUPS_GENIE.indexOf(s);
                }
            }
            //if palpation inc clinical_exam because ir was assigned to that group
            if(e.getAttributeValue("id").toLowerCase().contains(Util.PALPATION)) {
                numExamsPerGroup[1]++;
            }
            numExamsPerGroup[index]++;
        }
        //total number
        numExamsPerGroup[Progress.NUM_GROUPS - 1] = obsNodeList.size();
    }

    public int[] getNumExams() {
        return numExamsPerGroup;
    }

    /**
     * Returns the JDOM element containing the cases.
     * IMPORTANT: First read elements from file using: getNodes() !! or else genieDoc == null
     * @return the cases element
     */
    public Element getCases() {
        return genieDoc.getRootElement().getChild("cases");
    }

    /**
     * Saves the specified patient as a new node in the JDOM document and writes it to file.
     * A case is saved as follows:
     *
     * 	<case id="12345">
     * 		<evidence node="node_name_from_genie_file" state="findings">quality</evidence>
     * 		...
     * 		<softevidence node="node_name_from_genie_file" state="findings">0.11 0.22 0.33 0.44</softevidence>
     * 		...
     * 	</case>
     *
     * @param patient The patient to save.
     */
    public void savePatient(Patient patient) {
        log.info("Saving patient with ID " + patient.getID() + "...");
        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        boolean existingPatient = false;
        Element caseElement = null;
        //search for patient to see of there is already an existing patient with that ID
        for(Element patientElement : genieDoc.getRootElement().getChild("cases").getChildren("case")) {
            //if existing patient -> load data, set existingPatient = true
            if(patientElement.getAttributeValue("id").equals(""+patient.getID())) {
                existingPatient = true;
                log.info("Found patient in file!");
                caseElement = patientElement;
                //remove existing patient from file after copying
                genieDoc.getRootElement().getChild("cases").getChildren("case").remove(patientElement);
                writeToFile(xmlOutput);
                break;
            }
        }
        //else create new patient node with corresponding ID
        if(!existingPatient) {
            log.info("Patient wasn't found. Saving as new one.");
            caseElement = new Element("case");
            caseElement.getAttributes().add(new Attribute("id", ""+patient.getID()));
            if(!(patient.getName() == null) && !(patient.getBirthDate() == null)) {
                if(patient.getName().contains("ä")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ä", "ae")));
                } else if(patient.getName().contains("ö")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ö", "oe")));
                } else if(patient.getName().contains("ü")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ü", "ue")));
                } else {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName()));
                }
                caseElement.getAttributes().add(new Attribute("birthdate", patient.getBirthDate().toString()));
            }
        } else {
            log.info("Saving patient");
            if(!(patient.getName() == null) && !(patient.getBirthDate() == null)) {
                if(patient.getName().contains("ä")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ä", "ae")));
                } else if(patient.getName().contains("ö")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ö", "oe")));
                } else if(patient.getName().contains("ü")) {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName().replaceAll("ü", "ue")));
                } else {
                    caseElement.getAttributes().add(new Attribute("name", patient.getName()));
                }
                caseElement.getAttributes().add(new Attribute("birthdate", patient.getBirthDate().toString()));
            }
        }

        for(ExaminationGroup e : patient.getExamGroups()) {
            boolean exists = false;
            for(Element el : caseElement.getChildren("examgrp")) {
                if(el.getAttributeValue("name").equals(e.getName())) {
                    //TODO INFO: How to handle this case? genie dynamic bayesian networks
                    exists = true;
                }
            }
            if(!exists) {
                Element examGrp = new Element("examgrp");
                examGrp.getAttributes().add(new Attribute("name",e.getName()));
                examGrp.getAttributes().add(new Attribute("date",e.getDate().toString()));
                examGrp.getAttributes().add(new Attribute("place",e.getPlaceOfExam()));
                caseElement.getChildren().add(examGrp);
            }
        }

        //loop through all examinations
        for(Examination exam : patient.getExams()) {
            //if existingPatient see whether examination already exists
            if(existingPatient) {
                //loop through ALL children (evidence AND softevidence) to see whether examination is already existing
                List<Element> exams = new LinkedList<Element>();
                exams.addAll(caseElement.getChildren("evidence"));
                exams.addAll(caseElement.getChildren("softevidence"));
                for(Element examElement : exams) {
                    if(examElement.getAttribute("node").getValue().equals(exam.getName())) {
                        //if it exists delete old line
                        caseElement.getChildren().remove(examElement);
                        break;
                    }
                }
            }
            //save the examination to the patient node
            saveNode(exam, caseElement);
        }
        //save changes to file
        genieDoc.getRootElement().getChild("cases").addContent(caseElement);
        log.info("Writing changes to file...");
        writeToFile(xmlOutput);
        log.info("Finished writing changes to file!");
    }

    /**
     * Saves an examination to an existing case node
     * @param exam The examination to save.
     * @param caseElement The case node to save the examination to.
     */
    private void saveNode(Examination exam, Element caseElement) {
//        System.out.println("Examination to save: " + exam);
        Element newExam = null;
//        System.out.println("Examination to save: \n" + exam.toString());
        if(exam.hasSoftEvidence()) {
            //if exam has softevidence save softevidence node
            newExam = new Element("softevidence");
            newExam.getAttributes().add(new Attribute("node", exam.getName()));
//            newExam.getAttributes().add(new Attribute("date", exam.getDate().toString()));
            //exams don't have specified findings if soft evidences are provided...
//            newExam.getAttributes().add(new Attribute("state", exam.getFindingsComponent()));
            newExam.setText(""+exam.getSoftevidenceString());
        } else {
            //else save evidence node
            newExam = new Element("evidence");
            newExam.getAttributes().add(new Attribute("node", exam.getName()));
            //Fix NullPointerException: Can not set a null value for an Attribute happens with exams in "Others" group
            //Should be fixed ?! -> for now keep that IF -> could be integrated in log or something...
            if(exam.getFindings() != null) {
                newExam.getAttributes().add(new Attribute("state", exam.getFindings().getName()));
//                newExam.getAttributes().add(new Attribute("date", exam.getDate().toString()));
                newExam.setText(""+exam.getFindings().getProbability());
            } else {
                log.error("Findings == null: \n" + exam.toString() + "\n============\n");
            }
            //for evidence nodes the findings should be set -> get the certainty value
//            System.out.println(exam);
        }
        caseElement.getChildren().add(newExam);
    }

    /**
     * Writes changes to file.
     * @param xmlOutput The XMLOutputter to use.
     */
    private void writeToFile(XMLOutputter xmlOutput) {
        try {
            xmlOutput.output(genieDoc, new FileWriter(genieFile.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads all patients from file. Patients are represented by <case> nodes in the GeNIe file.
     *
     * @return A list of all patients.
     */
    public LinkedList<Patient> getPatientsFromFile() {
        log.info("Searching for available patients...");
        LinkedList<Patient> patients = new LinkedList<Patient>();
        Element casesElement = genieDoc.getRootElement().getChild("cases");
        List<Element> cases = casesElement.getChildren("case");
        for(Element caseEl : cases) {
            try {
                Patient p = new Patient(caseEl.getAttribute("id").getIntValue());
                //
                p.setName(caseEl.getAttributeValue("name"));
                p.setBirthDate(LocalDate.parse(caseEl.getAttributeValue("birthdate")));
                //examgrp nodes
                List<Element> examGrp = caseEl.getChildren("examgrp");
                for(Element el : examGrp) {
                    p.addExamGrp(new ExaminationGroup(el.getAttributeValue("name"), LocalDate.parse(el.getAttributeValue("date")),
                            el.getAttributeValue("place")));
                }
                //evidence nodes
                List<Element> exams = caseEl.getChildren("evidence");
                for(Element examEl : exams) {
//                    System.out.println(examEl.getAttributeValue("node"));
                    ExaminationGroup group = p.getGroupByName(findExamGroup(examEl.getAttributeValue("node")));
                    List<String> possibleFindings = getPossibleFindings(examEl.getAttributeValue("node"));
//                    System.out.println("  Possible Outcomes: " + possibleFindings);
//                    System.out.println("    Findings: " + examEl.getAttributeValue("state"));
//                    System.out.println("    Certainty: " + examEl.getText());
                    Examination exam = new Examination(examEl.getAttributeValue("node"), possibleFindings,
                            new Findings(examEl.getAttributeValue("state"), new Double(examEl.getText())));
//                    exam.setDate(LocalDate.parse(examEl.getAttributeValue("date")));
                    if(group != null) {
//                        System.out.println("    -> parsed exam: " + exam);
                        group.addExamination(exam);
//                        System.out.println(exam + "\n  -> patient: " + p);
                    } else {
                        //TODO INFO: handle this case -> if group wasn't found it returns "Others..." but if there is no node for
                        // that in the patient's file it fails to load that group...
                        log.error("No matching group found for: " + examEl.getAttributeValue("node"));
                    }
                }
                //softevidence nodes
                List<Element> examsVirtEv = caseEl.getChildren("softevidence");
                for(Element examEl : examsVirtEv) {
                    ExaminationGroup group = p.getGroupByName(findExamGroup(examEl.getAttributeValue("node")));
                    List<String> possibleFindings = getPossibleFindings(examEl.getAttributeValue("node"));
                    List<Double> softevidences = new LinkedList<Double>();
                    String softEvidencesString = examEl.getText();
                    int index = 1;
                    while(index > 0) {
                        index = softEvidencesString.indexOf(" ");
                        if(index != -1) {
                            String str = softEvidencesString.substring(0, index);
                            softevidences.add(Double.parseDouble(str));
                            softEvidencesString = softEvidencesString.substring(index+1);
                        } else {
                            softevidences.add(Double.parseDouble(softEvidencesString));
                        }
                    }
                    Examination exam = new Examination(examEl.getAttributeValue("node"), possibleFindings, softevidences);
//                    System.out.println("  Adding new examination: \n" + exam);
//                    exam.setDate(LocalDate.parse(examEl.getAttributeValue("date")));
                    //add examination to correct group
                    if(group != null) {
                        group.addExamination(exam);
                    } else {
                        //TODO INFO: handle this case -> if group wasn't found it returns "Others..." but if there is no node for
                        // that in the patient's file it fails to load that group...
                        log.error("No matching group found for: " + examEl.getAttributeValue("node"));
                    }
                }
                //set number of examinations in each group for the patient
//                for(int i = 0; i < Progress.NUM_GROUPS; i++) {
//                    p.getProgress().setNumExams(i, numExamsPerGroup[i]);
//                }
                p.getProgress().setNumExams(getNumExamsPerGroup());
                //set the progress of the examinations which were done for the patient
                for(ExaminationGroup eg : p.getExamGroups()) {
                    p.getProgress().setExamsDone(Util.getExamGroupIndexByName(eg.getName()), eg.getExaminations().size());
                }
                patients.add(p);
            } catch (DataConversionException e1) {
                e1.printStackTrace();
            }
        }
        log.info("Found & loaded " + patients.size() + " patients!");
        return patients;
    }

    /**
     * Finds the correct String for the name of an examination group given the name of an examination node
     * @param name The name of the node
     * @return
     */
    //eg "hep_M_Sono_GuidedPunction__patient"
    //This causes errors!
    private String findExamGroup(String name) {
        //if there is no match it should be assigned to the group "Others" because it has to be in any group!
        for(String s : Util.EXAMINATION_GROUPS_GENIE) {
            if(name.toLowerCase().contains(s)) {
                return Util.EXAMINATION_GROUPS.get(Util.EXAMINATION_GROUPS_GENIE.indexOf(s));
            } else if(name.toLowerCase().contains("palpation")) {
                //if group name contains "palpation" it is assigned to the "clinical_exam" group
                return Util.EXAMINATION_GROUPS.get(1);
            }
        }
        return "Others...";
    }

    /**
     * Searches for all possible findings of a certain examination.
     * @return
     */
    private LinkedList<String> getPossibleFindings(String exam) {
        LinkedList<String> possibleFindings = new LinkedList<String>();
        List<Element> nodes = genieDoc.getRootElement().getChild("nodes").getChildren("cpt");
        for(Element cpt : nodes) {
            if(cpt.getAttributeValue("id").equals(exam)) {
                for(Element state : cpt.getChildren("state")) {
                    possibleFindings.add(state.getAttributeValue("id"));
                }
            }
        }
        return possibleFindings;
    }

    /**
     * Gets the Genie file. This is mainly for testing purposes.
     * @return The genie file.
     */
    public File getGenieFile() {
        return genieFile;
    }

    /**
     * Returns the number of available examinations per group as an array.
     * 0  - PET
     * 1  - Clinical Examination & Palpation
     * 2  - Endoscopy
     * 3  - Intra OP
     * 4  - CT
     * 5  - MRI
     * 6  - Sono
     * 7  - Patho
     * 8  - Guided Punction
     * 9 - Others...
     * @return
     */
    public int[] getNumExamsPerGroup() {
        for(int i = 0; i < main.getExaminations().size(); i++) {
            numExamsPerGroup[i] = main.getExaminations().get(i).getExaminations().size();
        }
        return numExamsPerGroup;
    }
}
