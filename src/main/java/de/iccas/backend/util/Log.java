package de.iccas.backend.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

/**
 * Class to enable logging. This writes a seperate log file to the provided path.
 * @author Chris Unger, ICCAS, 2018
 */
//TODO OPTIONAL: Add possibility to hide certain log levels.
public class Log {

    private File logFile;

    private final boolean CLEAR_ON_STARTUP = true;

    /**
     * Constructor.
     * @param path
     */
    public Log(String path) {
        logFile = new File(path);
        //create new log file if none is available
        if(!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(CLEAR_ON_STARTUP) {
            clearLog();
        }
    }

    /**
     * Constructor.
     * @param logFile
     */
    public Log(File logFile) {
        this.logFile = logFile;
        if(CLEAR_ON_STARTUP) {
            clearLog();
        }
    }

    /**
     * Write an error message.
     * @param message
     */
    public void error(String message) {
        write("[ERROR] " + message);
    }

    /**
     * Write an info message.
     * @param message
     */
    public void info(String message) {
        write("[INFO] " + message);
    }

    /**
     * Writes a string to the log file.
     * @param message
     */
    private void write(String message) {
        Path file = Paths.get(logFile.getPath());
        try {
            List<String> lines = Files.readAllLines(file);
            lines.add("[" + LocalTime.now() + "]: " + message);
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clears the log.
     * @return
     */
    public boolean clearLog() {
        boolean success = true;
        Path file = Paths.get(logFile.getPath());
        try {
            Files.write(file, Arrays.asList(), Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }
}
