package de.iccas.backend.util;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class for the patient data tool. Gives some useful methods which are used throughout the program.
 * @author Chris Unger, ICCAS, 2016-2018
 */
public class Util {

    /**
     * The examination group names how they are listed on the buttons in the following order
     * 0  - PET
     * 1  - Clinical Examination & Palpation
     * 2  - Endoscopy
     * 3  - Intra OP
     * 4  - CT
     * 5  - MRI
     * 6  - Sono
     * 7  - Patho
     * 8  - Guided Punction
     * 9 - Others...
     */
    public static final List<String> EXAMINATION_GROUPS = Arrays.asList("PET", "Clinical Examination", "Endoscopy",
            "Intra OP", "CT", "MRI", "Sono", "Patho", "Guided Punction", "Others...");

    public static final List<String> EXAMINATION_GROUPS_GENIE = Arrays.asList("pet_", "clinical_exam", "endoscopy",
            "intra_op", "ct_", "mri_", "sono_", "patho", "guidedpunction");

    public static final String PALPATION = "palpation";

    /**
     * This is if there is no evidence for presence but it is also not completely impossible, prev.: "not examined"
     */
    public static final String UNSURE = "no evidence";

    /**
     * This is the option if there is definitely no sign of presence, prev. "nothing found"
     */
    public static final String DEFINITE_NO = "ruled out";

    /**
     * This is the option for examinations that were also done and the result will be added
     */
    public static final String TBA = "result will be added";

    /**
     * This is the state for examinations that were already set and are complete.
     */
    public static final String DONE = "Done";
    

    /**
     * Rounds a double value for the provided amount of digits and returns it.
     * @param value
     * @param digits
     * @return
     */
    public static double round(double value, int digits) {
        if (digits < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, digits);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * Returns a string representation of the Closest percentage of the quality of the findings.
     * @param quality The probability
     * @return
     */
    public static String getQualityString(double quality) {
        String qual = "-";
        if(quality >= 0.99) {
            return "99% - very sure";
        }
        if(quality >= 0.85) {
            return "85% - sure";
        }
        if(quality >= 0.75) {
            return "75% - mostly sure";
        }
        if(quality >= 0.5) {
            return "50% - 50/50";
        }
        if(quality >= 0.25) {
            return "25% - mostly unsure";
        }
        if(quality >= 0.15) {
            return "15% - unsure";
        }
        if(quality <= 0.01) {
            return "1% - very unsure";
        }
        return qual;
    }

    /**
     * Gets the index of the provided name of the examination group from the list of available ones.
     * @param name The examination group's name
     * @return The index of the group.
     * @throws IllegalArgumentException if name is not one of the available examination group names.
     */
    public static int getExamGroupIndexByName(String name) {
        if(EXAMINATION_GROUPS.contains(name)) {
            return EXAMINATION_GROUPS.indexOf(name);
        } else {
            throw new IllegalArgumentException("\"" + name + "\" was not found in the list of examination group names!");
        }
    }

    /**
     * Returns the substring the node must have according to the group it is in.
     * @param examGrp The name of the examination group
     * @return
     */
    public static String getGenieGroupStringByName(String examGrp) {
        if(examGrp.equals("PET")) {
            return "pet_";
        }
        if(examGrp.equals("Clinical Examination")) {
            return "clinical_exam";
        }
        if(examGrp.equals("Endoscopy")) {
            return "endoscopy";
        }
        if(examGrp.equals("Intra OP")) {
            return "intra_op";
        }
        if(examGrp.equals("CT")) {
            return "ct_";
        }
        if(examGrp.equals("MRI")) {
            return "mri_";
            //There are other possible node substrings... See examinationGroups.txt, sometimes mrt as well
        }
        if(examGrp.equals("Sono")) {
            return "sono_";
        }
        if(examGrp.equals("Patho")) {
            return "patho";
        }
        if(examGrp.equals("Palpation")) {
            return "palpation";
        }
        if(examGrp.equals("Guided Punction")) {
            return "guidedpunction";
        }
        return "";
    }

    /**
     * Returns the appropiate color for the selected group.
     * @param examGrp
     * @return
     */
    public static String getGroupColor(String examGrp) {
        if(examGrp.equals("PET")) {
            return "#99c2ff";
        }
        if(examGrp.equals("Clinical Examination")) {
            return "#0066ff";
        }
        if(examGrp.equals("Endoscopy")) {
            return " #003380";
        }
        if(examGrp.equals("Intra OP")) {
            return "#ff9999";
        }
        if(examGrp.equals("CT")) {
            return "#cc0000";
        }
        if(examGrp.equals("MRI")) {
            return "#660000";
        }
        if(examGrp.equals("Sono")) {
            return "#99ff99";
        }
        if(examGrp.equals("Patho")) {
            return " #00b300";
        }
        if(examGrp.equals("Palpation")) {
            return " #004d00";
        }
        if(examGrp.equals("Guided Punction")) {
            return " #ffcc80";
        }
        if(examGrp.equals("Others...")) {
            return " #ff9900";
        }
        return "ffffff";
    }
}
