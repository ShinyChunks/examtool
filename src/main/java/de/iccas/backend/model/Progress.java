package de.iccas.backend.model;

import de.iccas.backend.util.Util;

/**
 * Class for saving progress of added examinations for each examination group.
 * @author Chris Unger, ICCAS, 2016-2018
 */
public class Progress {

    public final static int OTHERS_INDEX = 9;

    private int[] examsDonePerGroup = {0, 0, 0,
                                       0, 0, 0,
                                       0, 0, 0,
                                       0,    0};

    private int[] numExamPerGroup = {0, 0, 0,
                                     0, 0, 0,
                                     0, 0, 0,
                                     0,    0};

    public static final int NUM_GROUPS = 11;


    /**
     * Updates the progress for the given examination group index by increasing one step.
     * @param examGroupIndex index of examination group -> see: Util.EXAMINATION_GROUPS
     * 0  - PET
     * 1  - Clinical Examination
     * 2  - Endoscopy
     * 3  - Intra OP
     * 4  - CT
     * 5  - MRI
     * 6  - Sono
     * 7  - Patho
     * 8  - Guided Punction
     * 9 - Others...
     * 10 - Total
     */
    public void incProgress(int examGroupIndex) {
        examsDonePerGroup[examGroupIndex]++;
        //update total progress
        examsDonePerGroup[NUM_GROUPS - 1]++;
    }

    /**
     * Gets the progress for the given examination group index.
     * 0  - PET
     * 1  - Clinical Examination
     * 2  - Endoscopy
     * 3  - Intra OP
     * 4  - CT
     * 5  - MRI
     * 6  - Sono
     * 7  - Patho
     * 8  - Guided Punction
     * 9  - Others...
     * 10 - Total
     * @param examGroupIndex index of examination group -> see: Util.EXAMINATION_GROUPS
     */
    public double get(int examGroupIndex) {
        return examsDonePerGroup[examGroupIndex] / ((double) numExamPerGroup[examGroupIndex]);
    }

    /**
     * Sets the number of examinations that were done in the given group.
     * @param examGroupIndex The index of the examination group.
     * @param numExamsDone The number of examinations done in the group.
     */
    public void setExamsDone(int examGroupIndex, int numExamsDone) {
        examsDonePerGroup[examGroupIndex] = numExamsDone;
    }

    /**
     * Returns the number of exams in the specified group.
     * @param examGrpIndex
     * @return
     */
    public int getNumExams(int examGrpIndex) {
        return numExamPerGroup[examGrpIndex];
    }

    /**
     *
     * @param examGrpIndex
     * @return
     */
    public int getNumExamsDone(int examGrpIndex) {
        return examsDonePerGroup[examGrpIndex];
    }

    /**
     * Sets the number of examinations in each group.
     * Please set the total number of examinations with the help of the genie file reader:
     * setNumExams(10, GeNIeFileReader.getObservableNodes().size());
     * @param examGroupIndex the index of the examination group to set the number for -> see get() for indexes
     * @param numberOfExams the number of examinations in this group
     */
    public void setNumExams(int examGroupIndex, int numberOfExams) {
        numExamPerGroup[examGroupIndex] = numberOfExams;
    }

    /**
     * Sets the number of examinations with an array.
     * @param numExamPerGroup
     */
    public void setNumExams(int[] numExamPerGroup) {
        this.numExamPerGroup = numExamPerGroup;
        //get total progress / number of examinations
        int sum = 0;
        for(int i = 0; i < Util.EXAMINATION_GROUPS.size(); i++) {
            sum += numExamPerGroup[i];
        }
        setNumExams(10, sum);
    }

    /**
     *
     */
    @Override
    public String toString() {
        String str = "";
        for(int i = 0; i < NUM_GROUPS; i++) {
            if(i < NUM_GROUPS - 1) {
                str += Util.EXAMINATION_GROUPS.get(i) + ": " + examsDonePerGroup[i] + " of " + numExamPerGroup[i] + " done.\n";
            } else {
                str += "Total: " + examsDonePerGroup[i] + " of " + numExamPerGroup[i] + " done.\n";
            }
        }
        return str;
    }
}
