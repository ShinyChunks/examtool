package de.iccas.backend.model;

import de.iccas.backend.util.Util;

import java.time.LocalDate;
import java.util.LinkedList;

/**
 * The class representing a patient.
 * @author Chris Unger, ICCAS, 2016-2018
 */
public class Patient {

    /**
     * The patient's unique 5 digit ID.
     */
    private final int ID;

    /**
     * The patient's first name.
     */
    private String firstName;

    /**
     * The patient's last name.
     */
    private String lastName;

    /**
     * Tzhe patient's date of birth.
     */
    private LocalDate birthDate;

    /**
     * The list of examinations, i.e. groups of examinations, that were done for the patient.
     */
    private LinkedList<ExaminationGroup> examGroups;


    /**
     * The progress tracker of the patient.
     */
    private Progress progress = new Progress();

    private String gender;

    private LocalDate tumorBoardDate;

    /**
     * Constructor.
     * @param id A unique 5 digit patient id.
     */
    public Patient(int id) {
        this.ID = id;
        this.examGroups = new LinkedList<ExaminationGroup>();
        firstName = "";
        lastName = "";
        birthDate = LocalDate.now();
    }

    /**
     * Returns the patient ID
     * @return patient id
     */
    public int getID() {
        return this.ID;
    }

    /**
     * Returns the examination group corresponding to the provided name if it was found or null else.
     * @param name The name of the examination group.
     * @return
     */
    public ExaminationGroup getGroupByName(String name) {
        for(ExaminationGroup examGrp : examGroups) {
            if(examGrp.getName().equals(name)) {
                return examGrp;
            }
        }
        return null;
    }

    /**
     * Returns the examination group corresponding to the provided name if it was found or null else.
     * @param genieName The genie representation of the name of the examination group.
     * @return
     */
    public ExaminationGroup getGroupByGenieName(String genieName) {
        //get the index of the geniie representation of the examination group string
        int index = Util.EXAMINATION_GROUPS_GENIE.indexOf(genieName);
        //return the examination searched by the 'real' name
        return getGroupByName(Util.EXAMINATION_GROUPS.get(index));
    }

    /**
     * Adds an examination to the list of exams done for the patient. It also increments the progress
     * of the corresponding group ('Others...' if the group name wasn't found in the list of available groups.
     * @param exam The examination to be added.
     */
    public void addExamination(ExaminationGroup group, Examination exam) {
        //Check whether the group already exists
        if(examGroups.contains(group)) {
            //check whether the examination already exists
            boolean alreadyAdded = false;
            for(Examination e : group.getExaminations()) {
                if(exam.getName().equals(e.getName())) {
                    alreadyAdded = true;
                }
            }
            if(!alreadyAdded) {
                exam.setState(Util.DONE);
                examGroups.get(examGroups.indexOf(group)).addExamination(exam);
                progress.incProgress(Util.getExamGroupIndexByName(group.getName()));
            }
        } else {
            throw new IllegalArgumentException("The provided group was not found!");
        }
    }

    /**
     * Gets a list of all examinations regardless of the examination group they belong to.
     * @return The list of all exams.
     */
    public LinkedList<Examination> getExams() {
        LinkedList<Examination> allExams = new LinkedList<Examination>();
        for(ExaminationGroup examGrp : examGroups) {
            allExams.addAll(examGrp.getExaminations());
        }
        return allExams;
    }

    /**
     * Returns the examination called "examName" from the list of all examinations regardless of the examination group
     * the examination belongs to, or null if no examination with that name was found.
     * @param examName The name of the examination to get.
     * @return The examination or null if not found.
     */
    public Examination getExaminationByName(String examName) {
        for(ExaminationGroup examGrp : examGroups) {
            for(Examination e : examGrp.getExaminations()) {
                if(e.getName().equals(examName)) {
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * Removes the examination group with the given name from the list of examination groups.
     * @param name
     */
    public void removeExaminationGroupByName(String name) {
        for(ExaminationGroup examGrp : examGroups) {
            if(examGrp.getName().equals(name)) {
                examGroups.remove(examGrp);
            }
        }
    }

    /**
     * Removes the given examination group from the list.
     * @param eg
     */
    public void removeExaminationGroup(ExaminationGroup eg) {
        examGroups.remove(eg);
    }

    /**
     * Returns the node for the examination with the given name if found, or null else.
     * @param name The name of the examination to find.
     * @return The node or null if there is none with that name.
     */
    public Examination getExaminationTBAByName(String name) {
        for(ExaminationGroup examGrp : examGroups) {
            for(Examination e : examGrp.getExaminations()) {
                if(e.getState().equals(Util.TBA) && e.getName().equals(name)) {
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * Getter for the list of exams, i.e. the groups of exams done for the patient.
     * @return List of all examination groups with exams done to the patient.
     */
    public LinkedList<ExaminationGroup> getExamGroups() {
        return examGroups;
    }

    /**
     * Gets the patient's full name.
     * @return full name: firstName lastName
     */
    public String getName() {
        return firstName + " " + lastName;
    }

    /**
     * Gets the patient's first name.
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the patient's last name.
     * @param
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets the patient's first name.
     * @param
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the patient's last name.
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the name of the patient.
     * @param firstName The first name.
     * @param lastName The last name.
     */
    public void setName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Sets the name of the patient.
     * @param name The full name of the patient: firstName lastName
     */
    public void setName(String name) {
        this.firstName = name.substring(0, name.indexOf(" "));
        this.lastName = name.substring(name.indexOf(" ") + 1);
    }

    /**
     *
     * @return
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     *
     * @param birthDate
     */
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets the progress tracker of the patient.
     * @return
     */
    public Progress getProgress() {
        //update progress (TODO: find better solution and remove previously setting methods...)
        for(ExaminationGroup eg : examGroups) {
            progress.setExamsDone(Util.getExamGroupIndexByName(eg.getName()), eg.getExaminations().size());
        }
        return progress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @param examGroup
     * @retun log message
     */
    public String addExamGrp(ExaminationGroup examGroup) {
        boolean alreadyAdded = false;
        for(ExaminationGroup eg : examGroups) {
            if(examGroup.getName().equals(eg.getName())) {
                alreadyAdded = true;
            }
        }
        if(!alreadyAdded) {
            examGroups.add(examGroup);
            progress.setExamsDone(Util.getExamGroupIndexByName(examGroup.getName()), examGroup.getExaminations().size());
            return "Added " + examGroup.getName() + " to the patient.";
        } else {
            //TODO Update exam grp?
            ExaminationGroup existingGroup = getExamGrpByName(examGroup.getName());
            existingGroup.update(examGroup);
            return "Examination group was already added! Skipping...";
        }
    }

    /**
     * Gets the examination group from the list of available examination groups.
     * @param name The name of the examination group to get.
     * @return The examination group or null if it wasn't found.
     */
    public ExaminationGroup getExamGrpByName(String name) {
        ExaminationGroup group = null;
        for(ExaminationGroup eg : examGroups) {
            if(eg.getName().equals(name)) {
                group = eg;
                break;
            }
        }
        return group;
    }

    /**
     * Gets a short one line string representation of the basic patient info
     * @return
     */
    public String getShortInfo() {
        return ID + ": " + firstName + " " + lastName + " (" + birthDate + ")";
    }

    /**
     *
     * @return
     */
    public LocalDate getTumorBoardDate() {
        return tumorBoardDate;
    }

    /**
     *
     * @param tumorBoardDate
     */
    public void setTumorBoardDate(LocalDate tumorBoardDate) {
        this.tumorBoardDate = tumorBoardDate;
    }

    /**
     * Whether the patient was part of a tumor board already
     * @return
     */
    public boolean hasTumorBoardEntry() {
        return !(tumorBoardDate==null);
    }

    /**
     *
     */
    @Override
    public String toString() {
        String str = "Patient-ID: " + ID + "\nName: " + firstName + " " + lastName + "\nDate of birth: " + birthDate + "\n\n";
        for(ExaminationGroup examGrp : examGroups) {
            str += examGrp.toString() + "\n";
            str += "----------------------------\n";
            for(Examination e : examGrp.getExaminations()) {
                str += "  - " + e.toString() + "\n";
            }
            str += "============================\n";
        }
        return str;
    }
}
