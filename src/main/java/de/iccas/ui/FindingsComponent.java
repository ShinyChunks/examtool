package de.iccas.ui;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.themes.ValoTheme;
import de.iccas.backend.model.Examination;
import de.iccas.backend.model.Findings;

import java.util.ArrayList;

/**
 * Class to bundle the findings element in the table of examinations.
 * It has to be a set of 3 radio buttons if the possible findings are "yes", "no" and "unknown" or a combo box
 * of all possible findings.
 *
 * @author Chris Unger, ICCAS, 2018
 */
public class FindingsComponent extends com.vaadin.ui.HorizontalLayout {

    /**
     * The examination.
     */
    private Examination exam;

    /**
     * The examination item that holds the findings component.
     * This is needed for updating the UI.
     */
    private ExaminationItem examItem;

    private boolean hasComboBox;

    public static final int MIN_WIDTH = 150;

    private  RadioButtonGroup<String> btGroup;

    private ComboBox<String> comboBox;

    public static final String NO = "no";

    public static final String YES = "yes";

    public static final String UNKNOWN = "unknown";

    /**
     * Constructor.
     * @param exam The examination.
     */
    public FindingsComponent(Examination exam, ExaminationItem examItem) {
        this.exam = exam;
        this.examItem = examItem;

        setMargin(false);
        setSpacing(false);
        setWidth(MIN_WIDTH + "px");

        if(exam.getPossibleOutcomes().size() == 3 && exam.getPossibleOutcomes().getList().contains(YES)
                && exam.getPossibleOutcomes().getList().contains(NO)
                && exam.getPossibleOutcomes().getList().contains(UNKNOWN)) {
            hasComboBox = false;
            //create radio button group
            ArrayList<String> options = new ArrayList<String>(3);
            options.add(YES);
            options.add(NO);
            options.add(UNKNOWN);
            btGroup = new RadioButtonGroup<String>("", options);
            //add listeners
            btGroup.addSelectionListener(e -> {
                //if findings not set create new one
                if(exam.getFindings() == null) {
                    exam.setFindings(new Findings(btGroup.getSelectedItem().get(), 0.0));
                }
                //mark as done if certainty and findings were set
                examItem.enableSlider();
                //check for selection
                if(!examItem.getExamView().getSelectedItems().isEmpty()) {
//                    examItem.getExamView().removeSelectedItem(examItem);
                    examItem.getExamView().setFindingsForSelection(btGroup.getSelectedItem().get());
                }
            });
            btGroup.setStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
            //add it to the layout
            addComponent(btGroup);
        } else {
            hasComboBox = true;
            //create combo box
            comboBox = new ComboBox<String>("", exam.getPossibleOutcomes().getList());
            //add listener
            comboBox.addValueChangeListener(e -> {
                //if findings not set create new one
                if(exam.getFindings() == null) {
                    exam.setFindings(new Findings(comboBox.getSelectedItem().get(), 0.0));
                }
                //mark as done if certainty and findings were set
                examItem.enableSlider();
            });
            //add it to the layout
            addComponent(comboBox);
        }
    }

    /**
     * Sets the findings of the UI element.
     * @param findings
     */
    public void setValue(String findings) {
//        System.out.println(findings + "\npossible findigs:\n" + exam.getPossibleOutcomes().getList());
        if(findings == null) {
            System.out.println("  WARNING: findings == null");
        } else if(findings.equals("softevidences")) {
            //if exam has softevidences disable findings components and show a label instead
            if(hasComboBox) {
                comboBox.setEnabled(false);
                removeComponent(comboBox);
            } else {
                btGroup.setEnabled(false);
                removeComponent(btGroup);
            }
            addComponent(new Label("Soft evidences"));
        } else if(exam.getPossibleOutcomes().getList().contains(findings)) {
            if(!hasComboBox) {
                btGroup.setValue(findings);
            } else {
                comboBox.setValue(findings);
            }
        } else if(findings.equals(NO) && hasComboBox) {
            if(exam.getPossibleOutcomes().getList().contains("none")) {
                comboBox.setValue("none");
            }
        } else if(!findings.equals("")){
            //show some information that the value could not be set automatically -> at least put it into the log
            Notification.show("Could not set value automatically!",
                    "The findings of the examination \"" + exam.getName() + "\" could not be set automatically, " +
                            "because it doesn't have any option called " + NO + " or none. Please choose the correct " +
                            "findings by yourself.",
                    Notification.Type.TRAY_NOTIFICATION).setDelayMsec(20000);
            //TODO Maybe add a button to select the exam item
        }
    }

    /**
     * Gets the selected findings or an empty string if nothing was selected.
     * @return
     */
    public String get() {
        if(!hasComboBox) {
            return btGroup.getSelectedItem().isPresent()?btGroup.getSelectedItem().get():"";
        } else {
            return comboBox.getSelectedItem().isPresent()?comboBox.getSelectedItem().get():"";
        }
    }

    public boolean hasComboBox() {
        return hasComboBox;
    }
}
