package de.iccas.ui;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.LocalDateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;
import de.iccas.Main;
import de.iccas.backend.model.ExaminationGroup;
import de.iccas.backend.model.Patient;
import de.iccas.backend.model.Progress;
import de.iccas.backend.util.Preferences;
import de.iccas.backend.util.Util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.vaadin.ui.themes.ValoTheme.BUTTON_BORDERLESS;
import static com.vaadin.ui.themes.ValoTheme.TABLE_BORDERLESS;

/**
 * This class implements the neck UI class and provides the functionality, i.e. button handlers etc.
 * @author Chris Unger, ICCAS, 2018
 */
public class NeckView extends NeckUI implements View {

    /**
     * The main instance.
     */
    private Main main;

    private Image smiley;

    private ListDataProvider<Patient> dataProvider;

    private Grid<Patient> patientsGrid;

    private Label lbSelection;

    private int selectedPatientID;

    private Label lbHadTumorBoard;

    /**
     * The percentage of examination items that have to be set to change to the happy smiley.
     */
    private final double HAPPINESS_VALUE = 0.35;

//    private Binder<Patient> patientBinder;

    /**
     * Constructor.
     * @param instance The instance of the main class.
     */
    public NeckView(Main instance) {
        main = instance;
        addExamButtonListeners();

        //set current date
        lbDate.setValue(LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
        updateUI();

        addButtonListeners();
        styleButtons();
        //use fake information if enabled
        if (main.preferences.usesFakeMode()) {
            //use fake patient information
            tfFirstName.setValue("Patient");
            tfFirstName.setDescription("This is a fake name for anonymization purposes. You can change it if you " +
                    "want to use real patient information.");
            tfLastName.setValue(lbPatientID.getValue());
            tfLastName.setDescription("This is a fake name for anonymization purposes. You can change it if you "+
                    "want to use real patient information.");
            dfBirthDate.setValue(LocalDate.of(1960,1,1));
            dfBirthDate.setDescription("Change the date if you want to use a real one.");
            dfBirthDate.setEnabled(false);
        } else {
            //mark patient information as required
            tfFirstName.setRequiredIndicatorVisible(true);
            tfLastName.setRequiredIndicatorVisible(true);
            dfBirthDate.setRequiredIndicatorVisible(true);
        }

        //add listeners to patient information fields
        tfFirstName.addValueChangeListener(e -> {
            //TODO add a check for accepted input
            //check whether all information is provided and if so enable "next" button
            if(!tfFirstName.isEmpty() && !tfLastName.isEmpty() && !dfBirthDate.isEmpty()) {
                btNext.setEnabled(true);
            }
        });
        tfFirstName.setValueChangeMode(ValueChangeMode.EAGER);
        tfLastName.addValueChangeListener(e -> {
            //TODO add a check for accepted input
            //check whether all information is provided and if so enable "next" button
            if(!tfFirstName.isEmpty() && !tfLastName.isEmpty() && !dfBirthDate.isEmpty()) {
                btNext.setEnabled(true);
            }
        });
        tfLastName.setValueChangeMode(ValueChangeMode.EAGER);
        dfBirthDate.addValueChangeListener(e -> {
            //check whether all information is provided and if so enable "next" button
            if(!tfFirstName.isEmpty() && !tfLastName.isEmpty() && !dfBirthDate.isEmpty()) {
                btNext.setEnabled(true);
                //disable it to not allow changes of the birth date
                dfBirthDate.setEnabled(false);
            }
        });
        updateProgressBars();
    }

    /**
     * Styles the buttons and adds icons...
     */
    private void styleButtons() {

        btPrev.setIcon(new ThemeResource("icons/32/prev.png"));
        btPrev.addStyleName(BUTTON_BORDERLESS);
        btSearch.setIcon(new ThemeResource("icons/32/search.png"));
        btSearch.addStyleName(BUTTON_BORDERLESS);
        btList.setIcon(new ThemeResource("icons/32/list.png"));
        btList.addStyleName(BUTTON_BORDERLESS);
        btPrint.setIcon(new ThemeResource("icons/32/print.png"));
        btPrint.addStyleName(BUTTON_BORDERLESS);
        btNext.setIcon(new ThemeResource("icons/32/next.png"));
        btNext.addStyleName(BUTTON_BORDERLESS);
        hBoxNavButtons.setStyleName("button-bar");
    }

    /**
     * Updates the progress bars and their tooltips.
     */
    public void updateProgressBars() {
        // initialize progress bars with the total number of examinations in the group, set the progress and add a description.
        // 0) "PET", 1) "Clinical Examination", 2) "Endoscopy", 3) "Intra OP", 4) "CT", 5) "MRI", 6) "Sono", 7) "Patho",
        // 8) "Guided Punction", 9) "Others..." 10) Total progress
        List<ProgressBar> progressBars = Arrays.asList(pbPET, pbClinicalExam, pbEndo, pbIntraOp, pbCT, pbMRI, pbSono,
                pbPatho, /*pbPalpation,*/ pbGuidedPunction, pbOthers);
        float total = 0;
        for(int i = 0; i < progressBars.size(); i++) {
            float progress = (float)(main.getCurrentPatient().getProgress().get(i));
            progressBars.get(i).setValue(progress);
            total += progress;
            progressBars.get(i).setDescription("You have entered " + (int) (progress * 100) + "% of all examinations in this group.\n" +
                    "This is a total of " + (int) (progress * main.getCurrentPatient().getProgress().getNumExams(i)) +
                    " out of " + main.getCurrentPatient().getProgress().getNumExams(i) + " examinations.");
        }
        //total progress is average of all other progresses
        total = total / 10.f;

        //update total progress
        pbTotal.setValue(total);
        pbTotal.setDescription("You have entered " + (int) (total * 100) + "% of all examinations in all groups.\n" +
                "This is a total of " + (int)(total * main.getCurrentPatient().getProgress().getNumExams(Progress.NUM_GROUPS - 1)) +
                " out of " + main.getCurrentPatient().getProgress().getNumExams(Progress.NUM_GROUPS - 1)
                + " examinations.");
        //update smiley
        gridButtons.removeComponent(smiley);
        if(total >= HAPPINESS_VALUE) {
            smiley = new Image(""/*(int)(total * 100) + "%"*/, new ThemeResource("images/good.png"));
            smiley.setDescription("You have entered " + (int)(total * 100) + "% of all examinations!");
        } else if(total < HAPPINESS_VALUE) {
            smiley = new Image(""/*(int)(total * 100) + "%"*/, new ThemeResource("images/ok.png"));
            smiley.setDescription("You have entered only" + (int)(total * 100) + "% of all examinations! Enter more than " +
                    + (int)(HAPPINESS_VALUE * 100) + "% to make me happy.");
        } else {
            smiley = new Image(""/*(int)(total * 100) + "%"*/, new ThemeResource("images/bad.png"));
            smiley.setDescription("Enter more examinations to make me happier!");
        }
        smiley.setHeight("60px");
        smiley.setStyleName("smiley");
        gridButtons.addComponent(smiley,1, 3);
        gridButtons.setComponentAlignment(smiley, Alignment.MIDDLE_CENTER);
    }

    /**
     * Adds listeners to the navigational buttons.
     */
    private void addButtonListeners() {
        btPrev.addClickListener(e -> {
            //If the previously added patient hasn't any examinatons and it's birthday is "today" it was added as a
            //new patient but no data was entered, so the patient can be deleted.
            if(main.getCurrentPatient().getBirthDate().equals(LocalDate.now())
                    && main.getCurrentPatient().getExamGroups().isEmpty()) {
                main.removeCurrent();
                updateUI();
            } else {
                if(main.getCurrentPatientIndex() > 0) {
                    main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                    main.prevPatient();
                    updateUI();
                } else {
                    //show some kind of informational dialog that the first patient is already shown and disable button
                    Notification.show("This is already the first patient.", Notification.Type.WARNING_MESSAGE);
                    btPrev.setEnabled(false);
                }
            }
        });
        btNext.addClickListener(e -> {
           //save current patient, change to next patient OR create new patient and update UI
            main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
            main.nextPatient();
            updateUI();
            //if it was a new patient (no examinations) reset the progress bars and disable "Next" button
            if(main.getCurrentPatient().getExamGroups().isEmpty()) {
                resetProgress();
                btNext.setEnabled(false);
            }
            btPrev.setEnabled(true);
        });
        btPrint.addClickListener(e -> {
            showPrintWindow();
        });
        btSearch.addClickListener(e -> {
            showSearchWindow();
        });
        btList.addClickListener(e -> {
            showListWindow();
        });
    }

    /**
     * show a popup window with a list of available patients.
     */
    private void showListWindow() {
        Window window = new Window("List of available patients");
        Panel content = new Panel();
        content.setHeight("100%");
        VerticalLayout contentLayout = new VerticalLayout();
        dataProvider = new ListDataProvider<>(main.getPatients());
        patientsGrid = new Grid<>();
        patientsGrid.addStyleName(TABLE_BORDERLESS);
        patientsGrid.setDataProvider(dataProvider);
        //add id column
        patientsGrid.addColumn(patient -> patient.getID(), new NumberRenderer())
                .setCaption("ID")
                .setId("id");
        //add last name column
        patientsGrid.addColumn(patient -> patient.getLastName(), new HtmlRenderer())
                .setCaption("Last Name")
                .setId("lastName");
        //add first name column
        patientsGrid.addColumn(patient -> patient.getFirstName(), new HtmlRenderer())
                .setCaption("First Name")
                .setId("firstName");
        //add birthday column
        patientsGrid.addColumn(patient -> patient.getBirthDate(), new LocalDateRenderer())
                .setCaption("Date of birth")
                .setId("birthDate");
        addFilters();
        patientsGrid.setWidth("100%");
        patientsGrid.setHeight("100%");
        patientsGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        patientsGrid.addSelectionListener(e -> {
            Patient selection = patientsGrid.getSelectedItems().iterator().next();
            lbSelection.setValue(selection.getShortInfo());
            selectedPatientID = selection.getID();
        });
        GridLayout belowGrid = new GridLayout(2,1);
        belowGrid.setWidth("100%");
        lbSelection = new Label();
        lbSelection.setHeight("50px");
        lbSelection.setCaption("Selected patient:");
        Button btJump = new Button("Jump to selection");
        btJump.setWidth("250px");
        btJump.addClickListener(e -> {
            main.changeToPatient(selectedPatientID);
            updateUI();
            window.close();
        });
        belowGrid.addComponent(lbSelection, 0, 0);
        belowGrid.addComponent(btJump, 1, 0);
        belowGrid.setComponentAlignment(lbSelection, Alignment.MIDDLE_LEFT);
        belowGrid.setComponentAlignment(btJump, Alignment.MIDDLE_RIGHT);

        contentLayout.addComponents(patientsGrid, belowGrid);
        content.setContent(contentLayout);
        content.setSizeFull();
        window.setContent(content);
        window.center();
        main.getUI().addWindow(window);
        window.setModal(true);
        window.setWidth("80%");
        window.setHeight("80%");
    }

    /**
     * Adds filter fields to the header of the grid
     */
    private void addFilters() {
        HeaderRow filteringHeader = patientsGrid.appendHeaderRow();
        //id column
        TextField filterID = new TextField();
        filterID.setWidth("100%");
        filterID.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filterID.setPlaceholder("Filter");
        filterID.addValueChangeListener(event -> {
            dataProvider.setFilter(Patient::getID, id -> {
                if (id == null) {
                    return false;
                }
                //Set the language to german, as the names are most likely german
                String idString = ""+id;
                String filterString = event.getValue().toLowerCase();
                return idString.contains(filterString);
            });
        });
        filteringHeader.getCell("id")
                .setComponent(filterID);
        //last name column
        TextField filterLastName = new TextField();
        filterLastName.setWidth("100%");
        filterLastName.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filterLastName.setPlaceholder("Filter");
        filterLastName.addValueChangeListener(event -> {
            dataProvider.setFilter(Patient::getLastName, patName -> {
                if (patName == null) {
                    return false;
                }
                //Set the language to german, as the names are most likely german
                String patLower = patName.toLowerCase(Locale.GERMAN);
                String filterLower = event.getValue().toLowerCase(Locale.GERMAN);
                return patLower.contains(filterLower);
            });
        });
        filteringHeader.getCell("lastName")
                .setComponent(filterLastName);
        //first name column
        TextField filterFirstName = new TextField();
        filterFirstName.setWidth("100%");
        filterFirstName.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filterFirstName.setPlaceholder("Filter");
        filterFirstName.addValueChangeListener(event -> {
            dataProvider.setFilter(Patient::getFirstName, patName -> {
                if (patName == null) {
                    return false;
                }
                //Set the language to german, as the names are most likely german
                String patLower = patName.toLowerCase(Locale.GERMAN);
                String filterLower = event.getValue().toLowerCase(Locale.GERMAN);
                return patLower.contains(filterLower);
            });
        });
        filteringHeader.getCell("firstName")
                .setComponent(filterFirstName);
        //date of birth column
        TextField filterBirthDate = new TextField();
        filterBirthDate.setWidth("100%");
        filterBirthDate.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filterBirthDate.setPlaceholder("Filter");
        filterBirthDate.addValueChangeListener(event -> {
            dataProvider.setFilter(Patient::getBirthDate, birthDate -> {
                if (birthDate == null) {
                    return false;
                }
                //Set the language to german, as the names are most likely german
                String dateStringLower = birthDate.toString().toLowerCase(Locale.GERMAN);
                String filterLower = event.getValue().toLowerCase(Locale.GERMAN);
                return dateStringLower.contains(filterLower);
            });
        });
        filteringHeader.getCell("birthDate")
                .setComponent(filterBirthDate);
    }

    /**
     * Gets the text field for the filter.
     * @return
     */
    private TextField getColumnFilterField() {
        TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filter.setPlaceholder("Filter");
        return filter;
    }

    /**
     * show a popup window with a search field
     */
    private void showSearchWindow() {
        Window window = new Window("Search for patient");
        Panel content = new Panel();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setWidth("300px");
        contentLayout.setHeight("250px");

        HorizontalLayout textField = new HorizontalLayout();
        TextField tfSearch = new TextField("Patient ID");
        tfSearch.setMaxLength(5);
        tfSearch.setDescription("The patient's unique ID consists of 5 numbers and cannot start with a 0.");
        Label counter = new Label();
        counter.setValue(tfSearch.getValue().length() + " of " + tfSearch.getMaxLength());
        tfSearch.addValueChangeListener(e -> {
            //TODO add check for leading 0 and for numbers only
            int len = e.getValue().length();
            counter.setValue(len + " of " + tfSearch.getMaxLength());
        });
        tfSearch.setValueChangeMode(ValueChangeMode.EAGER);
        textField.addComponents(tfSearch, counter);

        Button btSearchID = new Button("Search");
        btSearchID.addClickListener(e -> {
            try {
                boolean found = main.changeToPatient(Integer.parseInt(tfSearch.getValue()));
                if(!found) {
                    Notification.show("Not found!","There was no patient found with the provided ID.",
                            Notification.Type.WARNING_MESSAGE);
                } else {
                    updateUI();
                    Notification.show("Found patient with ID: " + tfSearch.getValue());
                    window.close();
                }
            } catch(NumberFormatException ex) {
                Notification.show("Number Format Error","The patient ID consists of numbers only!",
                        Notification.Type.ERROR_MESSAGE);
            }
        });
        btSearchID.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        contentLayout.addComponents(textField, btSearchID);
        contentLayout.setComponentAlignment(textField, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(btSearchID, Alignment.MIDDLE_CENTER);
        content.setContent(contentLayout);
        window.setContent(content);
        window.center();
        main.getUI().addWindow(window);
        window.setModal(true);
    }

    /**
     * show new page with all patient information (printing dialog) or save to text file etc.
     */
    private void showPrintWindow() {
        Window window = new Window("Patient info");
        window.setWidth("80%");
        window.setHeight("80%");
        VerticalLayout contentLayout = new VerticalLayout();
        //TODO make it more visually pleasing at some time
        TextArea patientInfo = new TextArea("List of examinations that were done",
                main.getCurrentPatient().toString());
        patientInfo.setSizeFull();
        patientInfo.setHeight("500px");
        contentLayout.addComponent(patientInfo);
        window.setContent(contentLayout);
        window.center();
        main.getUI().addWindow(window);
        window.setModal(true);
    }

    /**
     * Resets the progress bars to 0
     */
    private void resetProgress() {
        List<ProgressBar> progressBars = Arrays.asList(pbPET, pbClinicalExam, pbEndo, pbIntraOp, pbCT, pbMRI, pbSono,
                pbPatho, /*pbPalpation,*/ pbGuidedPunction, pbOthers);
        for(ProgressBar pb : progressBars) {
            pb.setValue(0);
        }
    }

    /**
     * Initializes the UI with the correct general and patient specific information.
     */
    private void updateUI() {
        //set number of patients
        lbNumPatients.setValue("" + main.getPatients().size());
        //set index of currently viewed patient
        lbCurrentPatient.setValue("" + (main.getCurrentPatientIndex() + 1));

        //set patient specific information
        lbPatientID.setValue("" + main.getCurrentPatient().getID());
        tfFirstName.setValue(main.getCurrentPatient().getFirstName());
        tfLastName.setValue(main.getCurrentPatient().getLastName());
        dfBirthDate.setValue(main.getCurrentPatient().getBirthDate());
        //add label if patient had tumor board (TODO may be a button in the future)
        if(main.getCurrentPatient().hasTumorBoardEntry()) {
            lbHadTumorBoard = new Label(main.getCurrentPatient().getTumorBoardDate().toString());
            lbHadTumorBoard.setCaption("Tumor board on: ");
            hBoxLabelsBottom.addComponent(lbHadTumorBoard);
        } else if(lbHadTumorBoard != null) {
            //remove the label if the patient hadn't have a tumor board
            hBoxLabelsBottom.removeComponent(lbHadTumorBoard);
            lbHadTumorBoard = null;
        }
        updateProgressBars();
    }

    /**
     * Adds listeners to the examination buttons.
     * Check whether name and date of birth was entered before changing the view!
     */
    private void addExamButtonListeners() {
        btPET.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                //add exam group to patient
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(0)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(0)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(0));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btClinicalExam.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(1)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(1)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(1));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btEndo.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(2)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(2)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(2));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btIntraOp.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(3)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(3)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(3));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btCT.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(4)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(4)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(4));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btMRI.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(5)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(5)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(5));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btSono.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(6)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(6)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(6));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btPatho.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(7)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(7)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(7));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

//        btPalpation.addClickListener(e -> {
//            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
//                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
//                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
//                main.saveCurrentPatient();
//                main.navigator.navigateTo(Main.EXAMVIEW);
//                ExamView examView = (ExamView) main.navigator.getCurrentView();
//                examView.setGroup(Util.EXAMINATION_GROUPS.get(8));
//            } else {
//                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
//                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
//            }
//            updateProgressBars();
//        });

        btGuidedPunction.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(8)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(8)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(8));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });

        btOthers.addClickListener(e -> {
            if(!tfLastName.isEmpty() && !tfFirstName.isEmpty() && !dfBirthDate.isEmpty()) {
                main.getCurrentPatient().setName(tfFirstName.getValue(), tfLastName.getValue());
                main.getCurrentPatient().setBirthDate(dfBirthDate.getValue());
                main.saveCurrentPatient();
                if(main.getCurrentPatient().getExamGrpByName(Util.EXAMINATION_GROUPS.get(9)) == null) {
                    main.getCurrentPatient().addExamGrp(new ExaminationGroup(Util.EXAMINATION_GROUPS.get(9)));
                }
                main.navigator.navigateTo(Main.EXAMVIEW);
                ExamView examView = (ExamView) main.navigator.getCurrentView();
                examView.setGroup(Util.EXAMINATION_GROUPS.get(9));
            } else {
                Notification.show("The patient's full name and date of birth have to be entered correctly before" +
                        " adding examination results.", Notification.Type.WARNING_MESSAGE);
            }
        });
    }
}
