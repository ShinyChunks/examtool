package de.iccas.ui;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import de.iccas.Main;
import de.iccas.backend.model.Examination;
import de.iccas.backend.model.ExaminationGroup;
import de.iccas.backend.util.Util;
import org.vaadin.dialogs.ConfirmDialog;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.vaadin.ui.themes.ValoTheme.TABLE_BORDERLESS;

/**
 * The class that implements the functionality of the examination view, hence it extends ExamUI.
 *
 * @author Chris Unger, ICCAS, 2018
 */
public class ExamView extends ExamUI implements View {

    private String examGroup;

    /**
     * The main instance of the application.
     */
    protected Main main;

    /**
     * The neck view instance that called the exam view.
     */
    private NeckView neckView;

    /**
     * The list of examination items in the grid.
     */
    private List<ExaminationItem> examinations;

    /**
     * A header for the filter text fields
     */
    private HeaderRow filteringHeader;

    /**
     * The data provider.
     */
    private ListDataProvider<ExaminationItem> dataProvider;

    /**
     * The height of the rows in px.
     */
    private final int ROW_HEIGHT = 100;

    private LinkedList<ExaminationItem> examListTBA;

    public boolean notified = false;

    /**
     * Constructor.
     * @param instance The main instance.
     */
    public ExamView(Main instance, NeckView neckView) {
        main = instance;
        this.neckView = neckView;

        examListTBA = new LinkedList<>();

        gridExams.addStyleName(TABLE_BORDERLESS);
        gridExams.setSelectionMode(Grid.SelectionMode.MULTI);

        //preset exam date to current date
        dfDate.setValue(LocalDate.now());
        dfDate.setRequiredIndicatorVisible(true);
        cbWhere.setRequiredIndicatorVisible(true);
        //set internal as default
        //TODO Check why this isn't set!
        cbWhere.setValue("internal");

        //disable jump button until an examination was selected as to be added
        btJump.setEnabled(false);

        //add listeners for navigational buttons
        btCancel.addClickListener(e -> {
            //confirmation dialog and change only if confirmed!
            ConfirmDialog.show(main.getUI(), "Are you sure?",
                    "Are you sure you want to cancel? This means your current unsaved changes will be lost.",
                    "Yes, cancel",
                    "No, stay on this page",
                    new ConfirmDialog.Listener() {
                        @Override
                        public void onClose(ConfirmDialog confirmDialog) {
                            if(confirmDialog.isConfirmed()) {
                                //go back
                                main.navigator.navigateTo(Main.MAINVIEW);
                            } else {
                                //close confirmation dialog and stay on the page
                            }
                        }
                    }
            );
            //remove examination group if it was empty when pressing cancel button
            if(getCurrentPatientsExamGroup().getExaminations().isEmpty()) {
                main.getCurrentPatient().removeExaminationGroup(getCurrentPatientsExamGroup());
            }
        });
        btJump.addClickListener(e -> {
           //jump to next examination which is flagged as "to be added"
            for(ExaminationItem exIt : examinations) {
                if(exIt.getSelectedState().equals(Util.TBA)) {
                    //add item to queue of exams tba if not already added
                    if(!examListTBA.contains(exIt)) {
                        examListTBA.add(exIt);
                    }
                }
            }
//            gridExams.setSelectionMode(Grid.SelectionMode.SINGLE);
            //select first value in queue
            ExaminationItem select = examListTBA.poll();
            //disable button again, if there are no items that were marked as to be added
            if(examListTBA.isEmpty()) {
                btJump.setEnabled(false);
            }
            gridExams.getSelectionModel().select(select);
            gridExams.scrollTo(examinations.indexOf(select));
            //TODO disable button until values are set... (?)
        });
        btOK.addClickListener(e -> {
            gridExams.deselectAll();
            LinkedList<ExaminationItem> itemsWithMissingValues = new LinkedList<>();
           //save inputs and go back to previous screen; Check whether entries are valid and nothing is missing
            if(!(cbWhere.isEmpty() || cbWhere.getValue().equals("")) && !dfDate.isEmpty()) {
                ExaminationGroup examinationGroup = new ExaminationGroup(examGroup, dfDate.getValue(), cbWhere.getSelectedItem().get());
                for(ExaminationItem examItem : examinations) {
                    //add a new examination if all needed values were set
                    if (examItem.isAddable()) {
                        examinationGroup.addExamination(examItem.getExam());
                    } else if (!(examItem.getFindingsComponent().get().equals("")
                                && examItem.getCertaintyValue() == 0
                                && examItem.getSelectedState().equals(""))) {
                        itemsWithMissingValues.add(examItem);
                    }
                    main.getCurrentPatient().addExamGrp(examinationGroup);
                }
                //save
                savePatient();
                //Add confirm dialog if there are examinations marked as "to be added"
                if(!examListTBA.isEmpty()) {
//                    for(ExaminationItem exIt : examListTBA) {
//                        System.out.println(exIt.getExam().getName() + ";");
//                    }
                    ConfirmDialog.show(main.getUI(), "Are you sure?",
                            "Are you sure you want to save and leave this window? " +
                                    "There are still some examinations marked as to be added.",
                            "Yes, I'll add them later.",
                            "No, stay on this page",
                            new ConfirmDialog.Listener() {
                                @Override
                                public void onClose(ConfirmDialog confirmDialog) {
                                    if(confirmDialog.isConfirmed()) {
                                        //go back
                                        main.navigator.navigateTo(Main.MAINVIEW);
                                    } else {
                                        //close confirmation dialog and stay on the page
                                    }
                                }
                            }
                    );
                } else {
                    if(itemsWithMissingValues.isEmpty()) {
                        //go back
                        main.navigator.navigateTo(Main.MAINVIEW);
                    } else {
                        showExamWarningWindow(itemsWithMissingValues);
                    }
                }
            } else {
                //show warning that fields are required
                Notification.show("Warning!", "Either the place of the examination or the " +
                        "date is missing. Please check and add those before continuing.", Notification.Type.WARNING_MESSAGE);
            }
            //update progress bars
            neckView.updateProgressBars();
        });
    }

    /**
     * Shows a warning pop-up that indicates missing values.
     * @param items The examination items which have missing values.
     */
    private void showExamWarningWindow(LinkedList<ExaminationItem> items) {
        Window window = new Window("Missing values!");
//        window.setWidth("60%");
//        window.setHeight("30%");
        VerticalLayout contentLayout = new VerticalLayout();
        TextArea taItems = new TextArea("Items with missing values:");
        for(ExaminationItem ex : items) {
            taItems.setValue(taItems.getValue() + ex.getExam().getName() + "\n");
        }
        //TODO Relative values would be preferred...
        taItems.setHeight("200px");
        taItems.setWidth("500px");
        Button jump = new Button("Select first...");
        jump.addClickListener(e -> {
            gridExams.getSelectionModel().select(items.getFirst());
            gridExams.scrollTo(examinations.indexOf(items.getFirst()));
            window.close();
        });
        contentLayout.addComponents(taItems, jump);
        contentLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        window.setContent(contentLayout);
        window.center();
        main.getUI().addWindow(window);
        window.setModal(true);
    }

    /**
     * Sets the examination group this view shows and updates the UI accordingly.
     * @param examGroup
     */
    public void setGroup(String examGroup) {
        this.examGroup = examGroup;
        //update UI
        lbExamGroup.setValue("<strong><font size=\"6\" color=\"" + Util.getGroupColor(examGroup) + "\">" + examGroup + "</strong>");
        lbExamGroup.setStyleName("center");
        //set place and date of exam if exam grp is available
        if(main.getCurrentPatient().getExamGrpByName(examGroup) != null) {
            cbWhere.setSelectedItem(main.getCurrentPatient().getExamGrpByName(examGroup).getPlaceOfExam());
            dfDate.setValue(main.getCurrentPatient().getExamGrpByName(examGroup).getDate());
        }
        init();
    }

    /**
     * Inits the grid with all the examinations from this group.
     */
    private void init() {
        examinations = new LinkedList<ExaminationItem>();
        //add all examinations from the group as a examination item to the list of examinations which is shown in the UI
//        System.out.println(main.getExaminations().get(Util.EXAMINATION_GROUPS.indexOf(examGroup)).getExaminations());
        for(Examination e : main.getExaminations().get(Util.EXAMINATION_GROUPS.indexOf(examGroup)).getExaminations()) {
            //load patient specific data / values if available, else create 'blank' examination item
            // 1) The patient has to have some examination groups done.
            // 2) The specific examination group has to be available for the current patient.
            // 3) The specific examination has to be available for the patient.
            if(!main.getCurrentPatient().getExamGroups().isEmpty() &&
                    main.getCurrentPatient().getExamGrpByName(examGroup) != null &&
                    main.getCurrentPatient().getExamGrpByName(examGroup).findByName(e.getName()) != null) {
                ExaminationItem item = new ExaminationItem(main.getCurrentPatient().getExamGrpByName(examGroup)
                        .findByName(e.getName()), this);
                //mark as done
                item.setState(Util.DONE);
                examinations.add(item);
            } else {
                examinations.add(new ExaminationItem(e, this));
            }
        }
        //set data source
        dataProvider = new ListDataProvider<>(examinations);
        gridExams.setDataProvider(dataProvider);
        gridExams.setBodyRowHeight(ROW_HEIGHT);

        //add name column
        if(gridExams.getColumn("name") != null) {
            gridExams.removeColumn("name");
            gridExams.addComponentColumn(examinationItem -> {
                return examinationItem.getName();
            }).setCaption("Name").setId("name");
        }

        setColumnFiltering(true);

        //add the findings column
        if(gridExams.getColumn("findings") == null) {
            gridExams.addComponentColumn(examinationItem -> {
                return examinationItem.getFindingsComponent();
            }).setCaption("Findings").setId("findings");
        }
        gridExams.getColumn("findings").setMinimumWidth(FindingsComponent.MIN_WIDTH);
        //set the comparator for the findings column to make it sortable
        //examinations with missing findings should be on top
        gridExams.getColumn("findings").setComparator((exam1, exam2) -> {
            if(exam1.getExam().getFindings() != null && exam2.getExam().getFindings() == null) {
                return -1;
            } else if(exam1.getExam().getFindings() == null && exam2.getExam().getFindings() != null) {
                return 1;
            } else {
                return 0;
            }
        });

        //add the certainty column
        if(gridExams.getColumn("certainty") == null) {
            gridExams.addComponentColumn(examinationItem -> {
                return examinationItem.getCertainty();
            }).setCaption("Certainty").setId("certainty");
        }
        gridExams.getColumn("certainty").setMinimumWidth(ExaminationItem.SLIDER_MIN_WIDTH + 30);
        //set the comparator for the certainty column to make it sortable
        gridExams.getColumn("certainty").setComparator((exam1, exam2) -> {
            if(exam1.getExam().getFindings() != null && exam2.getExam().getFindings() != null) {
                if(exam1.getExam().getFindings().getProbability() < exam2.getExam().getFindings().getProbability()) {
                    return -1;
                } else if(exam1.getExam().getFindings().getProbability() == exam2.getExam().getFindings().getProbability()) {
                    return 0;
                } else {
                    return 1;
                }
            } else if(exam1.getExam().getFindings() == null) {
                return -1;
            } else if(exam2.getExam().getFindings() == null) {
                return 1;
            } else {
                return 0;
            }
        });
//
//        //add the date column
//        if(gridExams.getColumn("date") == null) {
//            gridExams.addComponentColumn(examinationItem -> {
//                return examinationItem.getDateField();
//            }).setCaption("Date").setId("date");
//        }
//        gridExams.getColumn("date").setComparator((exam1, exam2) -> {
//            if(exam1.getDateField().getValue().isBefore(exam2.getDateField().getValue())) {
//                return -1;
//            } else if(exam1.getDateField().getValue().isAfter(exam2.getDateField().getValue())) {
//                return 1;
//            } else {
//                return 0;
//            }
//        });

        //add the status column
        if(gridExams.getColumn("status") == null) {
            gridExams.addComponentColumn(examinationItem -> {
                return examinationItem.getState();
            }).setCaption("Status").setId("status");
        }
        gridExams.getColumn("status").setComparator((exam1, exam2) -> {
            if(exam1.getExam().getState() == null) {
                return -1;
            } else if(exam2.getExam().getState() == null) {
                return 1;
            } else if(exam1.getExam().getState().equals(Util.TBA) || exam2.getExam().getState().equals(Util.TBA)) {
                if(exam1.getExam().getState().equals(Util.TBA) && !exam2.getExam().getState().equals(Util.TBA)) {
                    return 1;
                } else if(exam1.getExam().getState().equals(Util.TBA) && exam2.getExam().getState().equals(Util.TBA)) {
                    return 0;
                } else if(!exam1.getExam().getState().equals(Util.TBA) && exam2.getExam().getState().equals(Util.TBA)) {
                    return -1;
                }
            } else {
                if(exam1.getExam().getState().equals(Util.DONE) && !exam2.getExam().getState().equals(Util.DONE)) {
                    return 1;
                } else if(exam1.getExam().getState().equals(Util.DONE) && exam2.getExam().getState().equals(Util.DONE)) {
                    return 0;
                } else if(!exam1.getExam().getState().equals(Util.DONE) && exam2.getExam().getState().equals(Util.DONE)) {
                    return -1;
                }
            }
            return 0;
        });

        //set correct order in case something has been messed up
        gridExams.setColumnOrder("name", "findings", "certainty",/* "date",*/ "status");
        gridExams.getColumn("findings").setMinimumWidthFromContent(true);
        gridExams.getColumn("findings").setMaximumWidth(270);
        gridExams.getColumn("certainty").setMinimumWidthFromContent(true);
        gridExams.getColumn("certainty").setMaximumWidth(250);
//        gridExams.getColumn("date").setMinimumWidthFromContent(true);
//        gridExams.getColumn("date").setMaximumWidth(200);
        gridExams.getColumn("status").setMinimumWidthFromContent(true);
        gridExams.getColumn("status").setMaximumWidth(300);
    }

    /**
     * Adds filtering header(s) to the grid.
     * @param filtered true - filtering enabled
     *                 false - filtering disabled
     */
    private void setColumnFiltering(boolean filtered) {
        if (filtered) {
            if(filteringHeader == null) {
                filteringHeader = gridExams.appendHeaderRow();
            }

            // Add new TextFields to each column which filters the data from
            // that column
            TextField filteringField = getColumnFilterField();
            filteringField.addValueChangeListener(event -> {
                dataProvider.setFilter(ExaminationItem::getName, examName -> {
                    if (examName == null) {
                        return false;
                    }
                    String examLower = examName.getValue().toLowerCase(Locale.ENGLISH);
                    String filterLower = event.getValue().toLowerCase(Locale.ENGLISH);
                    return examLower.contains(filterLower);
                });
            });
            filteringHeader.getCell("name")
                    .setComponent(filteringField);
        } else if (!filtered && filteringHeader != null) {
            dataProvider.clearFilters();
            gridExams.removeHeaderRow(filteringHeader);
            filteringHeader = null;
        }
    }

    /**
     * Gets the text field for the name filter.
     * @return
     */
    private TextField getColumnFilterField() {
        TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addStyleName(ValoTheme.TEXTFIELD_TINY);
        filter.setPlaceholder("Filter");
        return filter;
    }

    /**
     * Refreshes the grid.
     */
    public void refreshGrid() {
        gridExams.getDataProvider().refreshAll();
    }

    /**
     * Saves the patient.
     */
    public void savePatient() {
        main.saveCurrentPatient();
    }

    /**
     * Gets the current patient.
     * @return
     */
    public ExaminationGroup getCurrentPatientsExamGroup() {
        return main.getCurrentPatient().getExamGrpByName(examGroup);
    }

    /**
     * Returns the selected items of the grid.
     * @return
     */
    public Set<ExaminationItem> getSelectedItems() {
        return gridExams.getSelectedItems();
    }

    /**
     * Deselects the selected item.
     */
    public void clearSelection() {
        gridExams.deselectAll();
    }

    /**
     * Updates all the selöected items with the given state.
     * @param state The state to select.
     */
    public void setStateForSelection(String state) {
        for(ExaminationItem item : getSelectedItems()) {
            item.setState(state);
        }
    }

    /**
     * Sets the findings for all selected items, if they have a buttonGroup
     * @param findings
     */
    public void setFindingsForSelection(String findings) {
        for(ExaminationItem item : getSelectedItems()) {
            item.setFindings(findings);
        }
    }

    /**
     * Sets the certainty for all selected items
     * @param certainty
     */
    public void setCertaintyForSelection(double certainty) {
        for(ExaminationItem item : getSelectedItems()) {
            if(!item.hasComboBox()) {
                item.setCertaintyValue(certainty);
            }
        }
    }

    /**
     * Removes the given item from the selected items.
     * @param item The item to remove
     */
    public void removeSelectedItem(ExaminationItem item) {
        gridExams.deselect(item);
    }

    /**
     * Adds an item to the list of examinations that will be added, if it not already contains it.
     * @param examTBA
     */
    public void addExamTBA(ExaminationItem examTBA) {
        if(!examListTBA.contains(examTBA)) {
            examListTBA.add(examTBA);
        }
    }

    /**
     * Removes the given examination item from the list of examinations that were marked as to be added, if it contains
     * the item.
     * @param name
     */
    public void removeExamTBA(String name) {
        for(ExaminationItem e : examListTBA) {
            if(e.getExam().getName().equals(name)) {
                examListTBA.remove(e);
            }
        }
    }
}
