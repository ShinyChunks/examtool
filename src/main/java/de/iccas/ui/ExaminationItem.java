package de.iccas.ui;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import de.iccas.backend.model.Examination;
import de.iccas.backend.model.Findings;
import de.iccas.backend.util.Util;

/**
 * The class which is a visual representation of an examination which is shown in the list of examinations
 * in the examination view of the application.
 * The table has 4 columns:
 *      - Examination (the name of the examination)
 *      - PossibleOutcomes (a FindingsComponent, which is a ComboBox or a group of 3 RadioButtons)
 *      - Certainty (a Slider from 0 to 100 % and a Button for soft evidences)
 *      - Status (an element to show the current status of the examination
 *
 * @author Chris Unger, ICCAS, 2018
 */
public class ExaminationItem {
    //TODO NICE_TO_HAVE: Add edit option to exam items! (-> change findingsComponent, certainty and type (evidence/softevidence))

    private Examination exam;

    private Label name;

    private Slider certaintySlider;

    private Button btSoftEvidence = new Button("SE");

    private FindingsComponent findingsComponent;

    private RadioButtonGroup<String> stateGroup;

//    private DateField dateField;

    private ExamView examView;

    /**
     * The predefined width of the slider.
     */
    public static final int SLIDER_MIN_WIDTH = 150;

    /**
     * The predefined certainty value that will be set if DEFINITE_NO was chosen as state.
     */
    private final double PREDEF_CERTAINTY_DEFINITE_NO = 0.9;

    /**
     * Constructor.
     * @param exam
     */
    public ExaminationItem(Examination exam, ExamView view) {
        this.exam = exam;
        this.examView = view;
        name = new Label(exam.getName());
        certaintySlider = new Slider();
        certaintySlider.setMax(100);
        certaintySlider.setMin(0);
        certaintySlider.setCaption(certaintySlider.getValue() + "%");
        certaintySlider.setWidth(SLIDER_MIN_WIDTH + "px");
        certaintySlider.addValueChangeListener(e -> {
            //update caption
            certaintySlider.setCaption(certaintySlider.getValue() + "%");
            //set certainty value for the examination
            exam.getFindings().setProbability(certaintySlider.getValue() / 100.d);
            //mark exam as done if there are findingsComponent
//            System.out.println("  Changed outcome of " + exam.getName() + " to " + exam.getFindings().getName() +
//                    " with a certainty of " + exam.getFindings().getProbability()*100 + "%.");
            if(exam.getFindings() != null && !exam.getFindings().equals("")) {
                exam.setState(Util.DONE);
                examView.removeExamTBA(exam.getName());
                //grey out status buttons
                stateGroup.setEnabled(false);
                stateGroup.setVisible(false);
                stateGroup.setDescription("The findings for this examination were already set. You have nothing to do here ;)");
                update();
            }
            if(!examView.getSelectedItems().isEmpty()) {
                examView.removeSelectedItem(this);
                examView.setCertaintyForSelection(certaintySlider.getValue());
            }
        });
        certaintySlider.setEnabled(false);
        certaintySlider.setDescription("Please enter the findings first first!");

        btSoftEvidence.setDescription("Soft evidences: Assign a probability to every single possible outcome...");
        btSoftEvidence.addClickListener(e -> {
            //handle soft evidence button click
            new SoftEvidenceView(view.main, this);
            //disable certainty slider
            certaintySlider.setEnabled(false);
            certaintySlider.setDescription("The slider was disabled because the examination has soft evidences.");
            //disable findingsComponent item because no findingsComponent have to be selected if soft evidences are provided
            findingsComponent.setEnabled(false);
            findingsComponent.setDescription("This was disabled, because the examination has soft evidences.");
            findingsComponent.setValue("");
            //TODO rethink how this is handled
            exam.setFindings(new Findings("softevidences", 0.0));
            //mark as done
            exam.setState(Util.DONE);
            //TODO maybe show the probabilities instead?
        });

        findingsComponent = new FindingsComponent(exam, this);

        stateGroup = new RadioButtonGroup<>();
//        stateGroup.setHtmlContentAllowed(true);
        stateGroup.setItems(Util.TBA, Util.UNSURE, Util.DEFINITE_NO);
        //if exam was selected as TBA set the state
        if(exam.getState() != null) {
            if(exam.getState().equals(Util.TBA)) {
                stateGroup.setSelectedItem(Util.TBA);
                update();
            }
        }
        //add listener
        stateGroup.addSelectionListener(e -> {
            exam.setState(stateGroup.getSelectedItem().get());
            examView.removeExamTBA(exam.getName());
            //TODO INFO: Notifications yes or no?!
            if(stateGroup.getSelectedItem().get().equals(Util.DEFINITE_NO)) {
                findingsComponent.setValue(FindingsComponent.NO);
                //set findings of exam
                exam.setFindings(new Findings(FindingsComponent.NO, PREDEF_CERTAINTY_DEFINITE_NO));
                setCertaintyValue(PREDEF_CERTAINTY_DEFINITE_NO * 100);
                //only show notification once
                if(!examView.notified) {
                    Notification.show("Ruled out selected",
                            "Please confirm the certainty of your statement by clicking this notification. " +
                                    "You can change it later. \nThe examination was: " + exam.getName(),
                            Notification.Type.WARNING_MESSAGE).setDelayMsec(-1);
                    examView.notified = true;
                }
                if(!examView.getSelectedItems().isEmpty()) {
                    examView.removeSelectedItem(this);
                    examView.setStateForSelection(Util.DEFINITE_NO);
                }
            } else if(stateGroup.getSelectedItem().get().equals(Util.UNSURE)) {
                enableSlider();
                findingsComponent.setValue(FindingsComponent.UNKNOWN);
                //set findings of examination
                //TODO maybe instead set probabilities to equal values?
                exam.setFindings(new Findings(FindingsComponent.UNKNOWN, 0.0));
                if(!examView.notified) {
                    Notification.show("No evidence selected",
                            "Please provide a certainty of your statement.\nThe examination was: " + exam.getName(),
                            Notification.Type.WARNING_MESSAGE).setDelayMsec(-1);
                    examView.notified = true;
                }
                if(!examView.getSelectedItems().isEmpty()) {
                    examView.removeSelectedItem(this);
                    examView.setStateForSelection(Util.UNSURE);
                }
            } else if(stateGroup.getSelectedItem().get().equals(Util.TBA)) {
                examView.addExamTBA(this);
                if(!examView.getSelectedItems().isEmpty()) {
                    examView.removeSelectedItem(this);
                    examView.setStateForSelection(Util.TBA);
                }
            }
            update();
        });

//        dateField = new DateField();
//        dateField.setValue(LocalDate.now());
//        dateField.setWidth("150px");
//        dateField.addValueChangeListener(e -> {
//            exam.setDate(dateField.getValue());
//        });

        // if the examination has patient specific information then update the UI elements accordingly
        if(exam.getFindings() != null || exam.hasSoftEvidence()) {
            setValues();
        }
    }

    /**
     * Sets the values of the UI elements according to the examination object.
     * Do this ony for examinations that are patient specific not completely new/empty ones!
     */
    private void setValues() {
        if(exam.hasSoftEvidence()) {
            findingsComponent.setValue("softevidences");
        } else {
            findingsComponent.setValue(exam.getFindings().getName());
            //slider has values from 0 to 100 and certainty is from 0.0 to 1.0 -> * 100
            setCertaintyValue(exam.getFindings().getProbability() * 100);
            certaintySlider.setCaption((int)(exam.getFindings().getProbability() * 100) + "%");
        }
        //check whether exam has a state
        if(exam.getState() != null) {
            if (exam.getState().equals(Util.DONE)) {
                stateGroup.setEnabled(false);
                stateGroup.setVisible(false);
            } else {
                stateGroup.setSelectedItem(exam.getState());
            }
            update();
        }
//        dateField.setValue(exam.getDate());
    }

    /**
     * Returns the name of the examination.
     * @return
     */
    public Label getName() {
        return name;
    }

    /**
     * Returns the fitting findingsComponent component (a set of 3 radio buttons or a combo box)
     * @return
     */
    public FindingsComponent getFindingsComponent() {
        return findingsComponent;
    }

    /**
     * Returns a horizontal layout containing the certainty slider and a button for soft evidences
     * @return
     */
    public HorizontalLayout getCertainty() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponents(certaintySlider, btSoftEvidence);
        layout.setComponentAlignment(certaintySlider, Alignment.MIDDLE_LEFT);
        layout.setComponentAlignment(btSoftEvidence, Alignment.MIDDLE_RIGHT);
        layout.setWidth("100%");
        return layout;
    }

    /**
     * Gets the selected certainty value.
     * @return
     */
    public double getCertaintyValue() {
        return certaintySlider.getValue();
    }

    /**
     * Sets the certainty of the item.
     * @param certainty
     */
    public void setCertaintyValue(double certainty) {
        certaintySlider.setValue(certainty);
    }

    /**
     * Returns a horizontal layout for the status component
     * @return
     */
    public HorizontalLayout getState() {
        HorizontalLayout layout = new HorizontalLayout();
        if(stateGroup.isVisible()) {
            layout.addComponent(stateGroup);
        } else {
            Label lbDone = new Label("<b><font color=\"green\">DONE</font></b>");
            lbDone.setContentMode(ContentMode.HTML);
            layout.addComponent(lbDone);
        }
        return layout;
    }

//    /**
//     *
//     * @return
//     */
//    public DateField getDateField() {
//        return dateField;
//    }
//
//    /**
//     *
//     * @param dateField
//     */
//    public void setDateField(DateField dateField) {
//        this.dateField = dateField;
//    }

    /**
     * Gets the selected state.
     * @return
     */
    public String getSelectedState() {
        return stateGroup.getSelectedItem().isPresent()?stateGroup.getSelectedItem().get():"";
    }

    /**
     * Gets the examination the exam item is representing.
     * @return
     */
    public Examination getExam() {
        return exam;
    }

    /**
     * Sets the status.
     * @param state
     */
    public void setState(String state) {
        //set state
        exam.setState(state);
        //select the state if it is not "Done"
        if(!state.equals(Util.DONE)) {
            stateGroup.setSelectedItem(state);
        } else {
            //if examination is done add it to the patient
            examView.getCurrentPatientsExamGroup().addExamination(exam);
        }
        update();
    }

    /**
     * Indicates whether the corresponding examination is ready to be added to the patient, i.e. all needed values are set.
     * @return
     */
    public boolean isAddable() {
        // The examination can be saved if it is done, i.e. the status group is disabled and every needed value was set
        // OR if a status was picked that is not TBA
        return ((!findingsComponent.get().isEmpty() &&
                    getCertaintyValue() != 0 &&
                    !stateGroup.isEnabled())
                || (stateGroup.getSelectedItem().isPresent() && !stateGroup.getSelectedItem().get().equals(Util.TBA)));
    }

    /**
     * Updates the colors of the labels according to the state.
     */
    public void update() {
        if(exam.getState().equals(Util.DONE)) {
            name.setStyleName("done");
            stateGroup.setEnabled(false);
            stateGroup.setVisible(false);
        } else if(exam.getState().equals(Util.TBA)) {
            name.setStyleName("tba");
            //enable jump button
            examView.btJump.setEnabled(true);
        } else if(exam.getState().equals(Util.DEFINITE_NO)) {
            name.setStyleName("definite-no");
        } else {
            name.setStyleName("no-evidence");
        }
        examView.refreshGrid();
    }

    /**
     * Enables the certainty slider.
     */
    public void enableSlider() {
        certaintySlider.setEnabled(true);
        certaintySlider.setDescription("The certainty of your findings.");
    }

    /**
     * Sets the findings of the corresponding findingsComponent
     * @param findings The findings to set
     */
    public void setFindings(String findings) {
        getFindingsComponent().setValue(findings);
        enableSlider();
    }

    public ExamView getExamView() {
        return examView;
    }

    public boolean hasComboBox() {
        return findingsComponent.hasComboBox();
    }
}
