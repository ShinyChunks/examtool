package de.iccas.ui;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.NumberRenderer;
import de.iccas.Main;
import de.iccas.backend.model.Findings;
import de.iccas.backend.util.Util;
import org.vaadin.dialogs.ConfirmDialog;

/**
 * The UI/view for the soft evidence edit dialog.
 * @author Chris Unger, ICCAS, 2018
 */
public class SoftEvidenceView extends VerticalLayout implements View {

    /**
     * The layout holding the three input methods: table, bar chart, pie chart
     */
    protected HorizontalLayout inputLayout;

    /**
     * The table for editing the probabilities of each possible outcome.
     */
    protected Grid<Findings> findingsGrid;

    /**
     * The layout holding the toolbar, i.e. the different functional buttons.
     */
    protected HorizontalLayout toolBar;

    /**
     * The button for scaling the probabilities, so that their sum equals 1.
     */
    protected Button btSum;

    /**
     * The button for computing the complement, i.e. 1-sum(p1...pk)
     */
    protected Button btComplement;

    /**
     * The layout holding the buttons on the bottom of the view.
     */
    protected HorizontalLayout buttonLayout;

    /**
     * The button to clear all probabilities from the table (and reset all charts respectively).
     */
    protected Button btClear;

    /**
     * The button to cancel. Returns to previous view without saving.
     */
    protected Button btCancel;

    /**
     * The button to save inputs and return to the previous view.
     */
    protected Button btOK;

    /**
     * The main UI.
     */
    private Main main;

    /**
     * The examination the view is shown for.
     */
    private ExaminationItem examItem;

    /**
     * The data provider.
     */
    private ListDataProvider<Findings> dataProvider;

    /**
     * The window of the examItem view.
     */
    private Window window;

    /**
     * Constructor.
     * @param mainInstance The instance of the main UI.
     */
    public SoftEvidenceView(Main mainInstance, ExaminationItem examItem) {
        this.main = mainInstance;
        this.examItem = examItem;
        initLayout();
        //add the components to the main layout
        addComponents(toolBar, inputLayout, buttonLayout);
        window = new Window("Virtual evidences...");
        window.setContent(this);
        window.center();
        main.getUI().addWindow(window);
    }

    /**
     * Initializes the content of the page.
     */
    private void initLayout() {
        toolBar = new HorizontalLayout();
        initToolbar();
        inputLayout = new HorizontalLayout();
        initInputLayout();
        buttonLayout = new HorizontalLayout();
        initButtonLayout();
    }

    /**
     * Initializes the layout holding the buttons on the bottom of the view.
     */
    private void initButtonLayout() {
        buttonLayout = new HorizontalLayout();
        btClear = new Button("Clear all probabilities");
        btClear.addClickListener(e -> {
            //Clear all probabilities and update the grid.
            for(Findings f : examItem.getExam().getPossibleOutcomes().getFindings()) {
                f.setProbability(0.0);
            }
            findingsGrid.getDataProvider().refreshAll();
        });
        buttonLayout.addComponent(btClear);

        btCancel = new Button("Cancel");
        btCancel.addClickListener(e -> {
            //Cancel -> close window without saving -> Confirm Dialog
            ConfirmDialog.show(main.getUI(), "Please confirm...",
                    "Are you sure you want to cancel? This means nothing from this page will be saved.",
                    "Yes, cancel",
                    "No, stay on this page",
                    new ConfirmDialog.Listener() {
                        @Override
                        public void onClose(ConfirmDialog confirmDialog) {
                            if(confirmDialog.isConfirmed()) {
                                //go back
                                window.close();
                            } else {
                                //close confirmation dialog and stay on the page
                            }
                        }
                    }
            );
        });
        buttonLayout.addComponent(btCancel);

        btOK = new Button("OK");
        btOK.addClickListener(e -> {
            //save values, scale if needed and close the window -> show a notification if values were scaled;
            //mark examItem as done
            if(sum() > 1) {
                scale();
                Notification.show("Scaling",
                        "The sum of probabilities was larger than 1, so it was scaled automatically!",
                        Notification.Type.WARNING_MESSAGE);
            }
            examItem.getExam().setSoftEvidences(examItem.getExam().getPossibleOutcomes().getProbabilities());
            examItem.setState(Util.DONE);
            //save patient
            main.saveCurrentPatient();
            //close window
            window.close();
        });
        buttonLayout.addComponent(btOK);
    }

    /**
     * Initializes the input layout with its grid and the two charts (-> TODO OPTIONAL: find solution for charts)
     */
    private void initInputLayout() {
        findingsGrid = new Grid<>();
        //init data source
        dataProvider  = new ListDataProvider<>(examItem.getExam().getPossibleOutcomes().getFindings());
        //add data source
        findingsGrid.setDataProvider(dataProvider);
        //set selection mode to single
        findingsGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        //add columns
        Grid.Column<Findings, String> nameCol = findingsGrid.addColumn(findings -> findings.getName());
        nameCol.setId("name");
        nameCol.setCaption("Name");
        Grid.Column<Findings, Double> probCol = findingsGrid.addColumn(Findings::getProbability, new NumberRenderer());
        probCol.setId("probability");
        probCol.setCaption("Probability");
        //add a selection listener
        findingsGrid.addSelectionListener(e -> {
            //enable complement button if there is a selected item
            if(!findingsGrid.getSelectedItems().isEmpty()) {
                btComplement.setEnabled(true);
            }
        });
        //enable editing
        findingsGrid.getEditor().setEnabled(true);
        //get binder
        Binder<Findings> binder = findingsGrid.getEditor().getBinder();
        //probability text field
        TextField probField = new TextField();
        //bind the textfield with the corresponding values
        Binder.Binding probBinding = binder.forField(probField).withConverter(
                        new StringToDoubleConverter("Must enter a number"))
                .bind(Findings::getProbability, Findings::setProbability);
        //set the binder of the column
        probCol.setEditorBinding(probBinding);
        //add component
        inputLayout.addComponent(findingsGrid);
    }

    /**
     * Initializes the toolbar with its buttons.
     */
    private void initToolbar() {
        toolBar.setStyleName("toolbar");
        btSum = new Button("&sum; = 1");
        btSum.setCaptionAsHtml(true);
        btSum.setDescription("Scales the probabilities, so that the sum of all probabilities is equal to 1.");
        btSum.addClickListener(e -> {
            scale();
        });
        toolBar.addComponent(btSum);

        btComplement = new Button("1 - &sum;");
        btComplement.setCaptionAsHtml(true);
        btComplement.setDescription("Calculates the complement of the probabilities, i.e. 1 minus the sum of all other" +
                " probabilities. This value is set for your selected item. Make sure to use this only if the scaling is correct!");
        btComplement.addClickListener(e -> {
            //compute sum and complement and set value
            //make sure that scaling is correct, i.e. sum < 1
            try{
                Findings selectedItem = (Findings)(findingsGrid.getSelectedItems().toArray()[0]);
                boolean valid = complement(selectedItem);
                if(!valid) {
                    Notification.show("Couldn't calculate complement!",
                            "The sum of all probabilities is larger then 1. Make sure that the sum of" +
                                    " probabilities is < 1 before trying to calculate the complement!",
                            Notification.Type.ERROR_MESSAGE);
                }
            } catch(ArrayIndexOutOfBoundsException error) {
                Notification.show("Error", "No item selected! Please select an item before trying to " +
                        "calculate the complement.", Notification.Type.ERROR_MESSAGE);
            }
        });
        btComplement.setEnabled(false);
        toolBar.addComponent(btComplement);
    }

    /**
     * Calculates the sum of probabilities and scales all probabilities so that the sum is equal to 1.
     * @return
     */
    private void scale() {
        double sum = sum();
        for(Findings f : examItem.getExam().getPossibleOutcomes().getFindings()) {
            f.setProbability(f.getProbability() / sum);
        }
        findingsGrid.getDataProvider().refreshAll();
    }

    /**
     * Calculates the sum.
     * @return
     */
    private double sum() {
        double sum = 0.0;
        for(Findings f : examItem.getExam().getPossibleOutcomes().getFindings()) {
            sum += f.getProbability();
        } return sum;
    }

    /**
     * Calculates the complement, i.e. 1 - sum of all OTHER probabilities.
     * Make sure to check that the sum is < 1!
     * @param selectedItem
     */
    private boolean complement(Findings selectedItem) {
        if((sum() - selectedItem.getProbability()) < 1) {
            selectedItem.setProbability(Util.round(1 - (sum() - selectedItem.getProbability()), 2));
            findingsGrid.getDataProvider().refreshAll();
            return true;
        } else {
            return false;
        }
    }
}
